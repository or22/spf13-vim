#!/usr/bin/env bash
############################################################
## Author:
##    Benjamin Chan <italkbenjamin@gmail.com>
##    http://www.3vtea.com
##
##    File:                    only_ln.sh
##    Create Date:      2015-02-11 09:57
############################################################
/bin/mkdir ~/.beforeUsingSpf13
/bin/mv ~/.bashrc ~/.beforeUsingSpf13/.bashrc.bak
/bin/mv ~/.inputrc ~/.beforeUsingSpf13/.inputrc.bak
/bin/mv ~/.vimrc* ~/.beforeUsingSpf13
/bin/mv ~/.vim ~/.beforeUsingSpf13
/bin/mv ~/.tmux.conf ~/.beforeUsingSpf13
/bin/mv ~/.tmux ~/.beforeUsingSpf13
/bin/mv ~/.vimperator/ ~/.beforeUsingSpf13
/bin/mv ~/.vimperatorrc ~/.beforeUsingSpf13
/bin/mkdir ~/.vim
sh -c "$(wget https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"
#curl -L http://install.ohmyz.sh | sh
/bin/ln -s ~/spf13-vim/vimrc_with_no_plugins ~/.vimrc
/bin/ln -s ~/spf13-vim/plugin/ ~/.vim/
/bin/ln -s ~/spf13-vim/colors/ ~/.vim/
/bin/ln -s ~/spf13-vim/templates ~/.vim/
/bin/ln -s ~/spf13-vim/.tmux.conf ~/
/bin/ln -s  ~/spf13-vim/.vimperator/ ~/.vimperator
/bin/ln -s  ~/spf13-vim/.vimperatorrc ~/.vimperatorrc
#ln -s ~/spf13-vim/.zshrc ~/
#mkdir ~/.antigen
#ln -s ~/spf13-vim/antigen/* ~/.antigen/
/bin/ln -s ~/spf13-vim/tmux ~/.tmux
/bin/ln -s ~/spf13-vim/.bashrc ~/.bashrc
/bin/ln -s ~/spf13-vim/.inputrc ~/.inputrc
/bin/ln -s  ~/spf13-vim/zshrc ~/.zshrc
source ~/.bashrc
source ~/.inputrc
