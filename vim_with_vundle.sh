#!/usr/bin/env bash
############################################################
## Author:
##    Benjamin Chan <italkbenjamin@gmail.com>
##    http://www.3vtea.com
##
##    File:                    only_ln.sh
##    Create Date:      2015-02-11 09:57
############################################################
rm -rf ~/.beforeUsingSpf13
rm -rf ~/spf13-vim/bundle
#mkdir ~/.antigen
/bin/mkdir ~/.beforeUsingSpf13
/bin/mv ~/.zshrc ~/.beforeUsingSpf13/.inputrc.bak
/bin/mv ~/.bashrc ~/.beforeUsingSpf13/.bashrc.bak
/bin/mv ~/.inputrc ~/.beforeUsingSpf13/.inputrc.bak
/bin/mv ~/.vimrc* ~/.beforeUsingSpf13
/bin/mv ~/.vim ~/.beforeUsingSpf13
/bin/mv ~/.tmux.conf ~/.beforeUsingSpf13
/bin/mv ~/.tmux ~/.beforeUsingSpf13
/bin/mv ~/.vimperator/ ~/.beforeUsingSpf13
/bin/mv ~/.vimperatorrc ~/.beforeUsingSpf13
sh -c "$(wget https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"
#curl -L http://install.ohmyz.sh | sh
/bin/mkdir ~/.vim
/bin/mkdir ~/spf13-vim/UNDO
/bin/ln -s ~/spf13-vim/vimrc_with_vundle ~/.vimrc
/bin/ln -s ~/spf13-vim/plugin/ ~/.vim/
/bin/ln -s ~/spf13-vim/colors/ ~/.vim/
/bin/ln -s ~/spf13-vim/templates ~/.vim/
/bin/ln -s ~/spf13-vim/bundle ~/.vim/
/bin/ln -s ~/spf13-vim/.tmux.conf ~/
/bin/ln -s ~/spf13-vim/.vimperator/ ~/.vimperator
/bin/ln -s  ~/spf13-vim/.vimperatorrc ~/.vimperatorrc
/bin/ln -s  ~/spf13-vim/jchen.zsh-theme ~/.oh-my-zsh/themes/
#ln -s ~/spf13-vim/antigen/* ~/.antigen/
/bin/ln -s ~/spf13-vim/tmux ~/.tmux
/bin/ln -s ~/spf13-vim/.bashrc ~/.bashrc
/bin/ln -s ~/spf13-vim/.inputrc ~/.inputrc
/bin/ln -s  ~/spf13-vim/zshrc ~/.zshrc
cd ~/spf13-vim && tar zxf bundle.tar.gz
cd ~/spf13-vim/bundle/YouCompleteMe && tar zxf ~/spf13-vim/YCM-git.tar.gz
cd ~/spf13-vim/bundle/YouCompleteMe/.git && tar zxf ~/spf13-vim/modules.tar.gz && tar zxf ~/spf13-vim/objects.tar.gz
git clone https://github.com/sorrowchen/spacemacs ~/.emacs.d
cd ~ && tar zxf ~/spf13-vim/spacemacs-zilongshanren.tar.gz
sudo /bin/ln -s ~/spf13-vim/Markdown.pl /usr/local/bin
sudo /bin/ln -s ~/spf13-vim/bin/nametags.sh /usr/local/bin/
#sudo apt-get install -y vim-ctrlp vim-common vim-doc vim-fugitive vim-gocomplete vim-snippets vim-ultisnips vim-tabular vim-youcompleteme
