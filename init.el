
(defun dotspacemacs/layers ()
  "Configuration Layers declaration.
You should not put any user code in this function besides modifying the variable
values."
  (setq-default
   dotspacemacs-distribution 'spacemacs
   dotspacemacs-configuration-layer-path '()
   dotspacemacs-configuration-layers
   '(
     (auto-completion :variables auto-completion-enable-sort-by-usage t
                      auto-completion-enable-help-tooltip t
                      )
     better-defaults
 (chinese :variables chinese-enable-youdao-dict t)
 (clojure :variables clojure-enable-fancify-symbols t)
     colors
     ; command-log
     ; fasd
     company
     deft
     emacs-lisp
     (git :variables
          git-magit-status-fullscreen t
          magit-push-always-verify nil
          magit-save-repository-buffers 'dontask
          magit-revert-buffers 'silent
          magit-refs-show-commit-count 'all
          magit-revision-show-gravatars nil)
     helm
     html
     (ibuffer :variables ibuffer-group-buffers-by 'projects)
     ivy
     markdown
     org
     prodigy
     (python :variables
             python-test-runner '(nose pytest))
     ranger
     search-engine
     (shell :variables
            shell-default-height 30
            shell-default-position 'bottom)
     (spacemacs-layouts :variables layouts-enable-autosave nil
                        layouts-autosave-delay 300)
     (syntax-checking :variables syntax-checking-enable-by-default t)
     (spell-checking :variables spell-checking-enable-by-default t)
     ; tmux
     version-control
     (vinegar :variables vinegar-reuse-dired-buffer t)
     yaml
     )
   dotspacemacs-additional-packages '(
                                      jira
                                      org-jira
                                      xml-rpc
                                      soap-client
                                      youdao-dictionary
                                      helm-dictionary
                                      define-word
                                      ; eww
                                      bing-dict
                                      ; edit-server
                                      dictionary
                                      ; jedi
                                      ; company-jedi
                                      )
   dotspacemacs-excluded-packages '()
   dotspacemacs-delete-orphan-packages t))

(defun dotspacemacs/init ()
  "Initialization function.
This function is called at the very startup of Spacemacs initialization
before layers configuration.
You should not put any user code in there besides modifying the variable
values."
  (setq-default
   dotspacemacs-elpa-https t
   dotspacemacs-elpa-timeout 5
   dotspacemacs-check-for-update t
   dotspacemacs-editing-style 'vim
   dotspacemacs-verbose-loading nil
   dotspacemacs-startup-banner nil
   dotspacemacs-startup-lists '(recents projects)
   dotspacemacs-startup-recent-list-size 5
   dotspacemacs-scratch-mode 'text-mode
   dotspacemacs-themes '(monokai  solarized-light spacemacs-light leuven zenburn)
   dotspacemacs-colorize-cursor-according-to-state t
   dotspacemacs-default-font '("Source Code Pro"
                               :size 13
                               :weight normal
                               :width normal
                               :powerline-scale 1.1)
   dotspacemacs-leader-key "SPC"
   dotspacemacs-emacs-leader-key "M-m"
   dotspacemacs-major-mode-leader-key ","
   dotspacemacs-major-mode-emacs-leader-key "C-M-m"
   dotspacemacs-distinguish-gui-tab nil
   dotspacemacs-command-key ":"
   dotspacemacs-remap-Y-to-y$ t
   dotspacemacs-default-layout-name "Default"
   dotspacemacs-display-default-layout nil
   dotspacemacs-auto-resume-layouts nil
   dotspacemacs-auto-save-file-location 'cache
   dotspacemacs-max-rollback-slots 5
   dotspacemacs-use-ido nil
   dotspacemacs-helm-resize nil
   dotspacemacs-helm-no-header nil
   dotspacemacs-helm-position 'bottom
   dotspacemacs-enable-paste-micro-state nil
   dotspacemacs-which-key-delay 0.4
   dotspacemacs-which-key-position 'bottom
   dotspacemacs-loading-progress-bar t
   dotspacemacs-fullscreen-at-startup nil
   dotspacemacs-fullscreen-use-non-native nil
   dotspacemacs-maximized-at-startup nil
   dotspacemacs-active-transparency 90
   dotspacemacs-inactive-transparency 90
   dotspacemacs-mode-line-unicode-symbols t
   dotspacemacs-smooth-scrolling t
   dotspacemacs-line-numbers nil
   dotspacemacs-smartparens-strict-mode nil
   dotspacemacs-highlight-delimiters 'all
   dotspacemacs-persistent-server nil
   dotspacemacs-search-tools '("ag" "pt" "ack" "grep")
   dotspacemacs-default-package-repository nil
   dotspacemacs-whitespace-cleanup nil
   ))

(defun dotspacemacs/user-init ()
  "Initialization function for user code.
It is called immediately after `dotspacemacs/init', before layer configuration
executes.
 This function is mostly useful for variables that need to be set
before packages are loaded. If you are unsure, you should try in setting them in
`dotspacemacs/user-config' first."
  (setq configuration-layer--elpa-archives
        '(("melpa-cn" . "http://elpa.zilongshanren.com/melpa/")
          ("org-cn"   . "http://elpa.zilongshanren.com/org/")
          ("gnu-cn"   . "http://elpa.zilongshanren.com/gnu/")))
  ;; https://github.com/syl20bnr/spacemacs/issues/2705
  ;; (setq tramp-mode nil)
  (setq tramp-ssh-controlmaster-options
        "-o ControlMaster=auto -o ControlPath='tramp.%%C' -o ControlPersist=no")
  ;; ss proxy. But it will cause anacond-mode failed.
  ;; (setq socks-server '("Default server" "127.0.0.1" 1080 5))
  (setq evil-shift-round nil)
  (setq byte-compile-warnings '(not obsolete))
  )

(defun dotspacemacs/user-config ()
  "Configuration function for user code.
This function is called at the very end of Spacemacs initialization after
layers configuration.
This is the place where most of your configurations should be done. Unless it is
explicitly specified that a variable should be set before a package is loaded,
you should place your code here."
  (defun bb/define-key (keymap &rest bindings)
    (declare (indent 1))
    (while bindings
      (define-key keymap (pop bindings) (pop bindings))))
  (bb/define-key evil-normal-state-map
    "+" 'evil-numbers/inc-at-pt
    "_" 'evil-numbers/dec-at-pt
    "\\" 'evil-repeat-find-char-reverse
    "[s" (lambda (n) (interactive "p") (dotimes (c n nil) (insert " ")))
    "]s" (lambda (n) (interactive "p")
           (forward-char) (dotimes (c n nil) (insert " ")) (backward-char (1+ n))))
  (bb/define-key company-active-map
    (kbd "C-w") 'evil-delete-backward-word)
  (bb/define-key company-active-map
    (kbd "s-w") 'company-show-location)
  (remove-hook 'emacs-lisp-mode-hook 'auto-compile-mode)
  ;; http://emacsredux.com/blog/2014/04/05/which-function-mode/
  (which-func-mode)
  ;; when editing js file, this feature is very useful
  (setq-default header-line-format
                '((which-func-mode ("" which-func-format " "))))
  (setq-default mode-line-misc-info
                (assq-delete-all 'which-function-mode mode-line-misc-info))
  (setq org-src-fontify-natively t)
  ;; show smartparens mode will cause Emacs frozen when use swiper...
  ;; (turn-off-show-smartparens-mode)
  ;; (add-hook 'emacs-lisp-mode-hook 'turn-on-show-smartparens-mode)
  ;; improve the performance of opening large file
  (add-hook 'org-mode-hook (lambda () (spacemacs/toggle-line-numbers-off)) 'append)
  (defun spacemacs/check-large-file ()
    (when (> (buffer-size) 500000)
      (progn (fundamental-mode)
             (hl-line-mode -1))))
  (add-hook 'find-file-hook 'spacemacs/check-large-file)
  (spacemacs/toggle-automatic-symbol-highlight-on)
  ;; For python
  (add-hook 'python-mode-hook #'(lambda () (modify-syntax-entry ?_ "w")))
  ;; For ruby
  (add-hook 'ruby-mode-hook #'(lambda () (modify-syntax-entry ?_ "w")))
  ;; For Javascript
  (add-hook 'js2-mode-hook #'(lambda () (modify-syntax-entry ?_ "w")))
  (spacemacs/set-leader-keys "ri" 'ivy-resume)
  (spacemacs|add-company-hook 'text-mode)
  (defadvice find-file (before make-directory-maybe
                               (filename &optional wildcards) activate)
    "Create parent directory if not exists while visiting file."
    (unless (file-exists-p filename)
      (let ((dir (file-name-directory filename)))
        (unless (file-exists-p dir)
          (make-directory dir t)))))
  (add-hook 'minibuffer-inactive-mode-hook
            '(lambda() (set (make-local-variable 'semantic-mode) nil)))
  (define-key global-map (kbd "<f4>") 'hydra-hotspots/body)
  (with-eval-after-load 'whitespace
    (diminish 'whitespace-mode))
  (with-eval-after-load 'smartparens
    (diminish 'smartparens-mode))
  (with-eval-after-load 'which-key
    (diminish 'which-key-mode))
  (with-eval-after-load 'hungry-delete
    (diminish 'hungry-delete-mode))
  ;; tips:  use diminish-undo to toggle mode l
  (if (configuration-layer/layer-usedp 'helm)
      (spacemacs/set-leader-keys "rh" 'helm-resume))
  (when (configuration-layer/layer-usedp 'helm)
    (spacemacs/set-leader-keys "sj" 'counsel-imenu))
  (setq-default ns-use-srgb-colorspace nil)
  (defun spaceline--unicode-number (str)
    "Return a nice unicode representation of a single-digit number STR."
    (cond
     ((string= "1" str) "➊")
     ((string= "2" str) "➋")
     ((string= "3" str) "➌")
     ((string= "4" str) "➍")
     ((string= "5" str) "➎")
     ((string= "6" str) "➏")
     ((string= "7" str) "➐")
     ((string= "8" str) "➑")
     ((string= "9" str) "➒")
     ((string= "0" str) "➓")))
  (defun window-number-mode-line ()
    "The current window number. Requires `window-numbering-mode' to be enabled."
    (when (bound-and-true-p window-numbering-mode)
      (let* ((num (window-numbering-get-number))
             (str (when num (int-to-string num))))
        (spaceline--unicode-number str))))
  (defun mode-line-fill (face reserve)
    "Return empty space using FACE and leaving RESERVE space on the right."
    (unless reserve
      (setq reserve 20))
    (when (and window-system (eq 'right (get-scroll-bar-mode)))
      (setq reserve (- reserve 3)))
    (propertize " "
                'display `((space :align-to
                                  (- (+ right right-fringe right-margin) ,reserve)))
                'face face))
  (defun zilongshanren/update-persp-name ()
    (when (bound-and-true-p persp-mode)
      ;; There are multiple implementations of
      ;; persp-mode with different APIs
      (progn
        (or (not (string= persp-nil-name (safe-persp-name (get-frame-persp))))
            "Default")
        (let ((name (safe-persp-name (get-frame-persp))))
          (propertize (concat "[" name "] ")
                      'face 'font-lock-preprocessor-face
                      'help-echo "Current Layout name.")))))
  (defun buffer-encoding-abbrev ()
    "The line ending convention used in the buffer."
    (let ((buf-coding (format "%s" buffer-file-coding-system)))
      (if (string-match "\\(dos\\|unix\\|mac\\)" buf-coding)
          (match-string 1 buf-coding)
        buf-coding)))
  (setq my-flycheck-mode-line
        '(:eval
          (pcase flycheck-last-status-change
            (`not-checked nil)
            (`no-checker (propertize " -" 'face 'warning))
            (`running (propertize " ✷" 'face 'success))
            (`errored (propertize " !" 'face 'error))
            (`finished
             (let* ((error-counts (flycheck-count-errors flycheck-current-errors))
                    (no-errors (cdr (assq 'error error-counts)))
                    (no-warnings (cdr (assq 'warning error-counts)))
                    (face (cond (no-errors 'error)
                                (no-warnings 'warning)
                                (t 'success))))
               (propertize (format "[%s/%s]" (or no-errors 0) (or no-warnings 0))
                           'face face)))
            (`interrupted " -")
            (`suspicious '(propertize " ?" 'face 'warning)))))
  (setq-default mode-line-format
                (list
                 " %1"
                 '(:eval (propertize
                          (window-number-mode-line)
                          'face
                          'font-lock-type-face))
                 " "
                 '(:eval (zilongshanren/update-persp-name))
                 "%1 "
                 ;; the buffer name; the file name as a tool tip
                 '(:eval (propertize "%b " 'face 'font-lock-keyword-face
                                     'help-echo (buffer-file-name)))
                 " [" ;; insert vs overwrite mode, input-method in a tooltip
                 '(:eval (propertize (if overwrite-mode "Ovr" "Ins")
                                     'face 'font-lock-preprocessor-face
                                     'help-echo (concat "Buffer is in "
                                                        (if overwrite-mode
                                                            "overwrite"
                                                          "insert") " mode")))
                 ;; was this buffer modified since the last save?
                 '(:eval (when (buffer-modified-p)
                           (concat ","  (propertize "Mod"
                                                    'face 'font-lock-warning-face
                                                    'help-echo "Buffer has been modified"))))
                 ;; is this buffer read-only?
                 '(:eval (when buffer-read-only
                           (concat ","  (propertize "RO"
                                                    'face 'font-lock-type-face
                                                    'help-echo "Buffer is read-only"))))
                 "] "
                 ;; anzu
                 anzu--mode-line-format
                 ;; relative position, size of file
                 "["
                 (propertize "%p" 'face 'font-lock-constant-face) ;; % above top
                 "/"
                 (propertize "%I" 'face 'font-lock-constant-face) ;; size
                 "] "
                 ;; the current major mode for the buffer.
                 '(:eval (propertize "%m" 'face 'font-lock-string-face
                                     'help-echo buffer-file-coding-system))
                 "%1 "
                 my-flycheck-mode-line
                 "%1 "
                 ;; evil state
                 '(:eval evil-mode-line-tag)
                 ;; minor modes
                 minor-mode-alist
                 " "
                 ;; git info
                 `(vc-mode vc-mode)
                 " "
                 ;; global-mode-string goes in mode-line-misc-info
                 mode-line-misc-info
                 (mode-line-fill 'mode-line 20)
                 ;; line and column
                 "(" ;; '%02' to set to 2 chars at least; prevents flickering
                 (propertize "%02l" 'face 'font-lock-type-face) ","
                 (propertize "%02c" 'face 'font-lock-type-face)
                 ") "
                 '(:eval (buffer-encoding-abbrev))
                 mode-line-end-spaces
                 ;; add the time, with the date and the emacs uptime in the tooltip
                 ;; '(:eval (propertize (format-time-string "%H:%M")
                 ;;                     'help-echo
                 ;;                     (concat (format-time-string "%c; ")
                 ;;                             (emacs-uptime "Uptime:%hh"))))
                 ))
  (spacemacs/set-leader-keys "en" 'flycheck-next-error)
  (spacemacs/set-leader-keys "ep" 'flycheck-previous-error)
  (spacemacs/set-leader-keys "o(" 'ielm)
  (spacemacs/set-leader-keys "gL" 'magit-log-buffer-file)
  (spacemacs/set-leader-keys "sj" 'helm-imenu)
  (with-eval-after-load 'clojure-mode
    (dolist (c (string-to-list ":_-?!#*"))
      (modify-syntax-entry c "w" clojure-mode-syntax-table )))
  ;; (dolist (mode '(erc-mode comint-mode term-mode eshell-mode inferior-emacs-lisp-mode))
    ;; (bb/remove-from-list evil-insert-state-modes mode))
  ;; deal with BOM
  (spacemacs/set-leader-keys "fl" 'find-file-literally-at-point)
  (setq auto-coding-regexp-alist
        (delete (rassoc 'utf-16be-with-signature auto-coding-regexp-alist)
                (delete (rassoc 'utf-16le-with-signature auto-coding-regexp-alist)
                        (delete (rassoc 'utf-8-with-signature auto-coding-regexp-alist)
                                auto-coding-regexp-alist))))
  (defun org-insert-src-block (src-code-type)
  "Insert a `SRC-CODE-TYPE' type source code block in org-mode."
  (interactive
   (let ((src-code-types
          '("emacs-lisp" "python" "C" "sh" "java" "js" "clojure" "C++" "css"
            "calc" "asymptote" "dot" "gnuplot" "ledger" "lilypond" "mscgen"
            "octave" "oz" "plantuml" "R" "sass" "screen" "sql" "awk" "ditaa"
            "haskell" "latex" "lisp" "matlab" "ocaml" "org" "perl" "ruby"
            "scheme" "sqlite")))
     (list (ido-completing-read "Source code type: " src-code-types))))
  (progn
    (newline-and-indent)
    (insert (format "#+BEGIN_SRC %s\n" src-code-type))
    (newline-and-indent)
    (insert "#+END_SRC\n")
    (previous-line 2)
    (org-edit-src-code)))
  (add-hook 'org-mode-hook '(lambda ()
                            ;; turn on flyspell-mode by default
                            (flyspell-mode 1)
                            ;; C-TAB for expanding
                            (local-set-key (kbd "C-<tab>")
                                           'yas/expand-from-trigger-key)
                            ;; keybinding for editing source code blocks
                            (local-set-key (kbd "C-c s e")
                                           'org-edit-src-code)
                            ;; keybinding for inserting code blocks
                            (local-set-key (kbd "C-c s i")
                                           'org-insert-src-block)
                            ))
  (defun ffap-hexl-mode ()
    (interactive)
    (let ((ffap-file-finder 'hexl-find-file))
      (call-interactively 'ffap)))
  (spacemacs/set-leader-keys "fh" 'ffap-hexl-mode)
  ;; if you use pyton3, then you could comment the following line
  (setq python-shell-interpreter "python")
  (add-hook 'prog-mode-hook 'spacemacs/toggle-visual-line-navigation-on)
  (add-hook 'dired-mode-hook 'deer)
  (add-hook 'makefile-mode-hook 'whitespace-mode)
  (add-hook 'text-mode-hook 'spacemacs/toggle-spelling-checking-on)
  ;; Custom Hooks
  (add-hook 'python-mode-hook
                 (lambda ()
                   (setq-local completion-at-point-functions nil)))
  ;; Custom Functions
  (defun bing-dict ()
 "Search current word in bing dictionary."
 (interactive)
 (save-restriction
 (let (start end)
 (skip-chars-backward "A-Za-z0–9") (setq start (point))
 (skip-chars-forward "A-Za-z0–9") (setq end (point))
 (setq current-word (buffer-substring start end))
 (eww (concat "http://cn.bing.com/dict/search?q=" current-word))
 (if (not (string= (buffer-name) "*eww*"))
 (switch-to-buffer-other-window "*eww*"))
 (hl-line-mode "*eww*")
 ;wait for 2 second, because the buffer will refresh soon and it go back to top line.
 (sit-for 0.4)
 (search-forward current-word nil t 2)
 ;mark the word for 1 second
 (end-of-line)
 (set-mark (line-beginning-position))
 (sit-for 0.4)
 (deactivate-mark)
 ))
)
  ;; Custom Veriables
  (setq google-translate-default-source-language "en")
  (setq google-translate-default-target-language "zh-CN")
  (setq paradox-github-token "8eae92f6bfef4ff2287495b28f2bfd969f25dfa1")
  (setq-default dotspacemacs-configuration-layers '((chinese :variables
                                                           chinese-enable-youdao-dict t)))
  (xterm-mouse-mode -1)
(setq dictionary-server "localhost")
(setq jiralib-url "https://jira.ehealthinsurance.com")
  ;; Custom Keybinds
(spacemacs/set-leader-keys
  "xgr" 'google-translate-at-point-reverse
  "xgg" 'google-translate-at-point)
  (spacemacs/set-leader-keys "oh" 'hydra-hotspots/body)
  (spacemacs/set-leader-keys "oo" 'youdao-dictionary-search-at-point+)
  (spacemacs/set-leader-keys "otm" 'xterm-mouse-mode)
  (spacemacs/set-leader-keys "otb" 'menu-bar-mode)
(spacemacs/set-leader-keys "od" 'dictionary-lookup-definition)
(spacemacs/set-leader-keys "os" 'dictionary-search)
(spacemacs/set-leader-keys "om" 'dictionary-match-words)
(define-key global-map (kbd "C-c C-y") 'bing-dict)
  ; (define-key global-map (kbd "C-c C-y") 'youdao-dictionary-search-at-point+)
  )
(setq custom-file (expand-file-name "custom.el" dotspacemacs-directory))
(load custom-file 'no-error 'no-message)
