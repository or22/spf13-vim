对于配置的加载顺序是：

	1. .vimrc.before -配置之前spf13-VIM
	2. .vimrc.before.fork -叉配置之前，
	3. .vimrc.before.local -用户配置之前，
	4. .vimrc.bundles - spf13-vim的配置包
	5. .vimrc.bundles.fork -叉包配置
	6. .vimrc.bundles.local -本地用户配置包
	7. .vimrc - spf13-VIM VIM配置
	8. .vimrc.fork -叉VIM配置
	9. .vimrc.local -本地用户配置


