#!/usr/bin/env bash
############################################################
## Author:
##    Benjamin Chan <italkbenjamin@gmail.com>
##    http://www.3vtea.com
##
##    File:                    nametags.sh
##    Create Date:      2015-08-19 14:24
############################################################
# generate tag file for lookupfile plugin
echo -e "!_TAG_FILE_SORTED/t2/t/2=foldcase/" > name.tags
find . -not -regex '.*/./(png/|gif/)' -type f -printf "%f/t%p/t1/n" | sort -f >> name.tags
