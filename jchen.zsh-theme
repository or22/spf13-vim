# justinchen.zsh-theme

# Use with a dark background and 256-color terminal!
# Tested only on Linux

# You can set your computer name in the ~/.box-name file if you want.

# Borrowing shamelessly from these oh-my-zsh themes:
#   fino-time as the main template
#   blinks for history num
#   half-life for lamda char
#   darkblood for pts
#   rgm for exit code
#   xiong_chiamiov-plus for time fomat

function box_name {
    [ -f ~/.box-name ] && cat ~/.box-name || echo ${SHORT_HOST:-$HOST}
}

autoload -U add-zsh-hook
autoload -Uz vcs_info

#use extended color pallete if available
if [[ $TERM = *256color* || $TERM = *rxvt* ]]; then
    turquoise="%F{81}"
    orange="%F{166}"
    purple="%F{135}"
    hotpink="%F{161}"
    limegreen="%F{118}"
else
    turquoise="$fg[cyan]"
    orange="$fg[yellow]"
    purple="$fg[magenta]"
    hotpink="$fg[red]"
    limegreen="$fg[green]"
fi
# enable VCS systems you use
zstyle ':vcs_info:*' enable git svn
zstyle ':vcs_info:*:prompt:*' check-for-changes true
local current_dir='${PWD/#$HOME/~}'
local git_info='$(git_prompt_info)'
local return_code="%(?..%{$fg[red]%}%? ↵%{$reset_color%})"

PROMPT="╭─%{$FG[040]%}%n%{$reset_color%}%{$FG[239]%}@%{$reset_color%}%{$FG[039]%}$(box_name)%{$reset_color%}%{$FG[239]%} %{$reset_color%}%{$terminfo[bold]$FG[226]%}${current_dir}%{$reset_color%} %{$FG[239]%}[/dev/%y%{$reset_color%}%{$FG[239]%}] !%{%!%} %{$FG[243]%} %*%{$reset_color%}
╰─${git_info}%{$orange%} λ %{$reset_color%}"

RPROMPT="%{$fg[red]%}${return_code}%{$reset_color%}"

PS2=$'%{$FG[023]%}|>%{$reset_color%}'

ZSH_THEME_GIT_PROMPT_PREFIX=" %{$FG[239]%}on%{$reset_color%} %{$fg[255]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_DIRTY="%{$FG[202]%}✘✘✘"
ZSH_THEME_GIT_PROMPT_CLEAN="%{$FG[040]%}✔"
