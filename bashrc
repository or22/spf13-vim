# .bashrc
#Source global definitions
if [ -f /etc/bashrc ]; then
    . /etc/bashrc
fi
# User specific aliases and functions
#PS1="[\u@\h \W]\\$ "
PS1="\! \w\\$ "
PS2="> "
PS4="+ "
#alias START
alias ..='cd ..'
alias ...='cd ../..'
alias beep='echo -en "\007"'
alias cd..='cd ..'
alias dir='ls -l'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
alias grep='grep --color=auto'
alias less='less -X'
alias df='df -hT'
alias free='free -m'
alias j=jobs
alias pu=pushd
alias po=popd
alias d='dirs -v'
alias h=history
alias grep=egrep
# List only file beginning with "."
alias lsa='ls -ld .* --color=auto --time-style=iso'
# List only directories and symbolic
# links that point to directories
alias lsd='ls -ld *(-/DN) --sort=version --color=auto --time-style=iso'
alias ll='ls -l --color=auto --time-style=iso' 2>/dev/null
alias ls='ls --color=auto --time-style=iso' 2>/dev/null
alias l='ls -alF --color=auto --time-style=iso' 2>/dev/null
alias li='ls -alFi --color=auto --time-style=iso' 2>/dev/null
alias la='ls -la --color=auto --time-style=iso' 2>/dev/null
alias md='mkdir -p'
alias cl='clear'
#alias mv='mv -i'
alias rm='rm -i'
#alias cp='cp -i'
alias o='less -X'
alias rd='rmdir'
alias rehash='hash -r'
alias unmount='echo "Error: Try the command: umount" 1>&2; false'
alias which='alias | /usr/bin/which --tty-only --read-alias --show-dot --show-tilde'
alias chf='find . -type f -exec chmod 644 {} \;'
alias chd='find . -type d -exec chmod 755 {} \;'
alias info='info --vi-keys'
#alias END

###################################################
export LANG=en_US.UTF-8
export TERM=xterm-256color
export EDITOR=vim
export PAGER="less -Xis"
export MANPAGER="less -Xis"
export HISTSIZE=100000
export HISTFILESIZE=409600
#export HISTIGNORE=":pwd*:id*:uptime*:which*:whereis*:locate*:uptime*:resize*:ls*:ll*:vim:fg*:python:netstat*:ps*:clear:history*:tree*:man*"
#export HISTCONTROL=ignoredups
export HISTCONTROL=erasedups
export WWW_HOME=http://www.baidu.com
#set -o vi
#shopt
#shopt -s extglob
#shopt -s nocaseglob
#SHELLOPTS
#export SHELLOPTS=braceexpand:hashall:histexpand:history:interactive-comments:monitor:vi
#export SHELLOPTS=braceexpand:hashall:histexpand:history:interactive-comments:monitor:emacs
###################################################
#alias xmwsf0='ssh -X -p54321 root@xmwsf0'
#alias lf0='ssh -X root@lf0'
#alias hf0='ssh -X root@hf0'
#alias lanf0='ssh -X root@lanf0'
#alias jmwsf0='ssh -X root@jmwsf0'
#alias centos71a='ssh -X root@centos71a'
#alias centos71b='ssh -X root@centos71b'
#alias centos71c='ssh -X root@centos71c'
#alias c6c1='ssh -X root@c6c1'
#alias c6c2='ssh -X root@c6c2'
#alias c6s='ssh -X root@c6s'
#alias f0='ssh -X root@f0'
#alias o1='ssh -X root@o1'
#alias o2='ssh -X root@o2'
#alias wanf0='ssh -X -p54321 justin@wanf0'
#alias psa='ps axwf -eo pid,user,cgroup,args'
#alias getip="ifconfig | grep 'inet addr' | cut -d: -f2 | cut -d' ' -f1"
#alias c0='ssh -X root@c0'
#alias s0='ssh -X root@s0'
#alias d0='ssh -X root@d0'
#alias tpon='xinput set-prop 11 "Device Enabled" 1'
#alias tpoff='xinput set-prop 11 "Device Enabled" 0'
#alias pxe='ssh root@pxe'
#alias dns1='ssh root@dns1'
#alias dns2='ssh root@dns2'
#alias ldap='ssh root@ldap'
#alias kdc='ssh root@kdc'
#alias ipa='ssh -X root@ipa'
#alias ipa2='ssh -X root@ipa2'
#alias email='ssh root@email'
#alias h0='ssh -X root@host0'
#alias k0='ssh -X root@kiosk0'
#alias h3='ssh -X root@host3'
#alias k3='ssh -X root@kiosk3'
#alias vm31='ssh -X root@vm31'
#alias vm32='ssh -X root@vm32'
#alias sam3='ssh -X root@sam3'
#alias rhs3='ssh -X root@rhs3'
#alias s1='ssh -X root@s1'
#alias s2='ssh -X root@s2'
#alias s3='ssh -X root@s3'
#alias s4='ssh -X root@s4'
#alias s5='ssh -X root@s5'
#alias s6='ssh -X root@s6'
#alias s7='ssh -X root@s7'
#alias g0='ssh -X root@g0'
#alias g1='ssh -X root@g1'
#alias g2='ssh -X root@g2'
#alias g3='ssh -X root@g3'
#alias g4='ssh -X root@g4'
#alias g5='ssh -X root@g5'
#alias g6='ssh -X root@g6'
#alias g7='ssh -X root@g7'
#alias g8='ssh -X root@g8'
#alias g9='ssh -X root@g9'
#alias fs='ssh -X root@fs'
#alias nfs='ssh -X root@nfs'
#alias ftp='ssh -X root@ftp'
#alias smb='ssh -X root@smb'
#alias www='ssh -X root@www'
#alias log='ssh -X root@log'
#alias mon='ssh -X root@mon'
#alias d1='ssh -X root@desktop1'
#alias dev1='ssh -X root@dev1'
#alias instructor='ssh -X root@instructor'
#Datong Global
#alias pc='ssh -X justin@pc'
#XMCU
alias dz1='ssh -X -p34567 root@dzxx75'
alias dz2='ssh -X -p34567 root@dzxx85'
alias dt-esxi-5='ssh root@dt-esxi-5'
alias dt-esxi-6='ssh root@dt-esxi-6'
alias dt-os-7='ssh -X root@dt-os-7'
alias dt-huidu='ssh root@dt-huidu'
alias dt-jira='ssh root@dt-jira' #commit bug
alias dt-svn='ssh root@dt-svn'
alias controller='ssh -X root@controller'
alias compute01='ssh -X root@compute01'
alias dt-ovirt-20='ssh -X root@dt-ovirt-20'
alias dt-ovirt-21='ssh -X root@dt-ovirt-21'
alias dt-ovirt-22='ssh -X root@dt-ovirt-22'
alias dt-ovirt-23='ssh -X root@dt-ovirt-23'
alias dt-ovirt-24='ssh -X root@dt-ovirt-24'
alias dt-ovirt-25='ssh -X root@dt-ovirt-25'
alias dt-ovirt-26='ssh -X root@dt-ovirt-26'
alias dt-ovirt-27='ssh -X root@dt-ovirt-27'
alias dt-ovirt-28='ssh -X root@dt-ovirt-28'
alias dt-ovirt-29='ssh -X root@dt-ovirt-29'
alias dt-ovirt-30='ssh -X root@dt-ovirt-30'
alias dt-ovirt-31='ssh -X root@dt-ovirt-31'
alias dt-ovirt-32='ssh -X root@dt-ovirt-32'
alias dt-ovirt-33='ssh -X root@dt-ovirt-33'
alias dt-ovirt-34='ssh -X root@dt-ovirt-34'
alias dt-ovirt-35='ssh -X root@dt-ovirt-35'
alias dt-ovirt-36='ssh -X root@dt-ovirt-36'
alias dt-env-dev='ssh root@dt-env-dev' #develope enviroment
alias dt-env-test12='ssh -X root@dt-env-test12' #test enviroment
alias dt-env-test21='ssh -X root@dt-env-test21' #automatic test
alias dt-env-test22='ssh -X root@dt-env-test22' #automatic test
alias dt-env-test23='ssh -X root@dt-env-test23' #automatic test
alias dt-env-test24='ssh -X root@dt-env-test24' #automatic test
alias dt-env-test25='ssh -X root@dt-env-test25' #automatic test
alias dt-env-test26='ssh -X root@dt-env-test26' #automatic test
alias dt-env-test27='ssh -X root@dt-env-test27' #automatic test
alias dt-op-zabbix='ssh -X root@dt-op-zabbix'
alias dt-cacti='ssh -X -i ~/chenj_hasee_ssh/id_rsa root@dt-cacti'
alias dt-op-push='ssh -X root@dt-op-push' #push server
alias dt-op-esxi='ssh -X root@dt-op-esxi'
alias dt-op-pc='ssh -X root@dt-op-pc'
alias dt-op-test60='ssh -X root@dt-op-test60'
alias dt-op-test61='ssh -X root@dt-op-test61'
alias dt-op-test65='ssh -X root@dt-op-test65'
alias dt-op-test66='ssh -X root@dt-op-test66'
alias dt-op-test67='ssh -X root@dt-op-test67'
alias dt-op-test68='ssh -X root@dt-op-test68'
alias dt-op-ovpn='ssh -X  root@dt-op-ovpn'
alias dt-op-smb='ssh -X root@dt-op-smb'
alias ovpn='cd /etc/openvpn ; sudo nohup openvpn dt-op-chenjian.ovpn & ;cd ~'
alias dt-op-online-mysql='ssh root@dt-op-online-mysql'
alias dt-op-lan-mysql='ssh root@dt-op-lan-mysql'
alias dt-op-lexiangbao='ssh -p 56789 root@www.lexiangbao.com'
alias dt-op-cap='ssh root@dt-op-cap'
alias dt-op-hongkong='ssh root@dt-op-hongkong'
alias simplehttp='sudo python -m SimpleHTTPServer 80 &'
