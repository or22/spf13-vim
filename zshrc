# Path to your oh-my-zsh installation.
  export ZSH=~/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
# ZSH_THEME="fino-time"
# ZSH_THEME="blinks"
# ZSH_THEME="half-life"
# ZSH_THEME="darkblood"
# ZSH_THEME="rgm"
# ZSH_THEME="sporty_256"
# ZSH_THEME="xiong-chiamiov-plus"
ZSH_THEME="jchen"

# ZSH_THEME="dpoggi"
# ZSH_THEME="philips"
# ZSH_THEME="pure"

# ZSH_THEME="dallas"
# ZSH_THEME="af-magic"
# ZSH_THEME="candy-kingdom"
# ZSH_THEME="candy"
# ZSH_THEME="jnrowe"

set COLORFGBG="green;black"
export COLORFGBG
setopt no_nomatch
#stty erase ^H
# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git)

# User configuration
export GOPATH=$HOME/go
export PATH=$PATH:$GOPATH/bin
export PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:~/bin:~/.bin"
# export MANPATH="/usr/local/man:$MANPATH"

source $ZSH/oh-my-zsh.sh
#source ~/.zshrc

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/dsa_id"
export LANG=zh_CN.UTF-8
export TERM=xterm-256color
export EDITOR=vim
export PAGER="less -Xis"
export MANPAGER="less -Xis"
export HISTSIZE=100000
export HISTFILESIZE=409600
#export HISTIGNORE=":pwd*:id*:uptime*:which*:whereis*:locate*:uptime*:resize*:ls*:ll*:vim:fg*:python:netstat*:ps*:clear:history*:tree*:man*"
#export HISTCONTROL=ignoredups
export HISTCONTROL=erasedups
#source ~/python3/bin/activate
#source ~/python2/bin/activate
export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"
#alias START
alias ..='cd ..'
alias ...='cd ../..'
alias beep='echo -en "\007"'
alias cd..='cd ..'
alias dir='ls -l'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
alias grep='grep --color=auto'
alias less='less -X'
alias df='df -hT'
alias free='free -m'
alias j=jobs
alias pu=pushd
alias po=popd
alias d='dirs -v'
alias h=history
alias grep=egrep
# List only file beginning with "."
alias lsa='ls -ld .* --color=auto --time-style=iso'
# List only directories and symbolic
# links that point to directories
alias lsd='ls -ld *(-/DN) --sort=version --color=auto --time-style=iso'
alias ll='ls -l --color=auto --time-style=iso' 2>/dev/null
alias ls='ls --color=auto --time-style=iso' 2>/dev/null
alias l='ls -alF --color=auto --time-style=iso' 2>/dev/null
alias li='ls -alFi --color=auto --time-style=iso' 2>/dev/null
alias la='ls -la --color=auto --time-style=iso' 2>/dev/null
alias md='mkdir -p'
alias cl='clear'
#alias mv='mv -i'
alias rm='rm -i'
#alias cp='cp -i'
alias o='less -X'
alias rd='rmdir'
alias rehash='hash -r'
alias unmount='echo "Error: Try the command: umount" 1>&2; false'
alias chf='find . -type f -exec chmod 644 {} \;'
alias chd='find . -type d -exec chmod 755 {} \;'
alias info='info --vi-keys'
alias minicom='minicom -c on'
#alias END
#alias f0="ssh -X root@172.25.0.250"
alias f0="ssh -X root@192.168.215.99"
alias nodea="ssh -X root@172.25.0.10"
alias nodeb="ssh -X root@172.25.0.11"
alias d0="ssh -X root@172.25.0.10"
alias s0="ssh -X root@172.25.0.11"
alias nodec="ssh -X root@172.25.0.12"
alias noded="ssh -X root@172.25.0.13"
alias rhevm="ssh -X root@rhevm"
alias workstation="ssh -X root@workstation"
alias emacs="nohup /usr/bin/emacs-25.* &"
alias cow="nohup cow &"
# RH401 BEGIN
alias sam="ssh -X root@sam"
alias rhs="ssh -X root@rhs"
alias kiosk="ssh -X root@kiosk"
alias host0="ssh -X root@host"
alias vm1="ssh -X root@vm1"
alias vm2="ssh -X root@vm2"
alias classroom="ssh -X root@classroom"
# RH401 END
#
alias freecache1='sync; sync; sync; sudo echo 3 >/proc/sys/vm/drop_caches'
alias freecache='sudo echo 0>/proc/sys/vm/drop_caches'
alias vmvmnet='sudo chmod 777 /dev/vmnet*'
alias mbak="sudo mount  -t cifs -ousername=chenj,password='aaaAAA111!!!@@@' //172.16.99.9/bak /mnt/bak"
alias mdatasets="sudo mount  -t cifs -ousername=chenj,password='aaaAAA111!!!@@@' //172.16.99.99/datasets /mnt/datasets"
alias mhp="sudo mount  -t cifs -ousername=chenj,password='aaaAAA111!!!@@@' //172.16.99.9/hp /mnt/hp"
alias mstorage="sudo mount  -t cifs -ousername=chenj,password='aaaAAA111!!!@@@' //172.16.99.9/storage /mnt/storage"
alias ubak="sudo umount /mnt/bak"
alias udatasets="sudo umount /mnt/datasets"
alias uhp="sudo umount /mnt/hp"
alias ustorage="sudo umount /mnt/storage"
#alias sp3='source ~/python3/bin/activate'
#alias sp2='source ~/python2/bin/activate'
alias dzxx75="ssh -X root@112.5.65.142 -p34567"
alias dzxx85="ssh -X root@112.5.65.143 -p34567"
if [ -z "$DISPLAY" ] && [ -n "$XDG_VTNR" ] && [ "$XDG_VTNR" -eq 1 ]; then
  exec startx
fi
