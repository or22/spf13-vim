# .bashrc
#Source global definitions
if [ -f /etc/bashrc ]; then
    . /etc/bashrc
fi
# User specific aliases and functions
#PS1="[\u@\h \W]\\$ "
#PS1="\! \w\\$ "
PS1=$(print "\033]0;${PWD}\n\033[32m${USER}@${HOST} \033[33m${PWD/${HOME}/~}\033[0m\n\\# ")
PS2="> "
PS4="+ "
#alias START
alias ..='cd ..'
alias ...='cd ../..'
alias beep='echo -en "\007"'
alias cd..='cd ..'
alias dir='ls -l'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
alias grep='grep --color=auto'
alias less='less -X'
alias df='df -hT'
alias free='free -m'
alias j=jobs
alias pu=pushd
alias po=popd
alias d='dirs -v'
alias h=history
alias grep=egrep
# List only file beginning with "."
alias lsa='ls -ld .* --color=auto --time-style=iso'
# List only directories and symbolic
# links that point to directories
alias lsd='ls -ld *(-/DN) --sort=version --color=auto --time-style=iso'
alias ll='ls -l --color=auto --time-style=iso' 2>/dev/null
alias ls='ls --color=auto --time-style=iso' 2>/dev/null
alias l='ls -alF --color=auto --time-style=iso' 2>/dev/null
alias li='ls -alFi --color=auto --time-style=iso' 2>/dev/null
alias la='ls -la --color=auto --time-style=iso' 2>/dev/null
alias md='mkdir -p'
alias cl='clear'
#alias mv='mv -i'
alias rm='rm -i'
#alias cp='cp -i'
alias o='less -X'
alias rd='rmdir'
alias rehash='hash -r'
alias unmount='echo "Error: Try the command: umount" 1>&2; false'
alias chf='find . -type f -exec chmod 644 {} \;'
alias chd='find . -type d -exec chmod 755 {} \;'
alias info='info --vi-keys'
#alias END

###################################################
export LANG=en_US.UTF-8
export TERM=xterm-256color
export EDITOR=vim
export PAGER="less -Xis"
export MANPAGER="less -Xis"
export HISTSIZE=100000
export HISTFILESIZE=409600
#export HISTIGNORE=":pwd*:id*:uptime*:which*:whereis*:locate*:uptime*:resize*:ls*:ll*:vim:fg*:python:netstat*:ps*:clear:history*:tree*:man*"
#export HISTCONTROL=ignoredups
export HISTCONTROL=erasedups
export WWW_HOME=http://www.baidu.com
#set -o vi
#shopt
#shopt -s extglob
#shopt -s nocaseglob
#SHELLOPTS
#export SHELLOPTS=braceexpand:hashall:histexpand:history:interactive-comments:monitor:vi
#export SHELLOPTS=braceexpand:hashall:histexpand:history:interactive-comments:monitor:emacs
###################################################
alias xmwsf0='ssh -X -p54321 root@xmwsf0'
alias lf0='ssh -X root@lf0'
alias hf0='ssh -X root@hf0'
alias lanf0='ssh -X root@lanf0'
alias jmwsf0='ssh -X root@jmwsf0'
alias centos71a='ssh -X root@centos71a'
alias centos71b='ssh -X root@centos71b'
alias centos71c='ssh -X root@centos71c'
alias c6c1='ssh -X root@c6c1'
alias c6c2='ssh -X root@c6c2'
alias c6s='ssh -X root@c6s'
alias f0='ssh -X root@f0'
alias o1='ssh -X root@o1'
alias o2='ssh -X root@o2'
alias wanf0='ssh -X -p54321 justin@wanf0'
alias psa='ps axwf -eo pid,user,cgroup,args'
alias getip="ifconfig | grep 'inet addr' | cut -d: -f2 | cut -d' ' -f1"
alias c0='ssh -X root@c0'
alias s0='ssh -X root@s0'
alias d0='ssh -X root@d0'
alias tpon='xinput set-prop 11 "Device Enabled" 1'
alias tpoff='xinput set-prop 11 "Device Enabled" 0'
alias pxe='ssh root@pxe'
alias dns1='ssh root@dns1'
alias dns2='ssh root@dns2'
alias ldap='ssh root@ldap'
alias kdc='ssh root@kdc'
alias ipa='ssh -X root@ipa'
alias ipa2='ssh -X root@ipa2'
alias email='ssh root@email'
alias h0='ssh -X root@host0'
alias k0='ssh -X root@kiosk0'
alias h3='ssh -X root@host3'
alias k3='ssh -X root@kiosk3'
alias vm31='ssh -X root@vm31'
alias vm32='ssh -X root@vm32'
alias sam3='ssh -X root@sam3'
alias rhs3='ssh -X root@rhs3'
alias s1='ssh -X root@s1'
alias s2='ssh -X root@s2'
alias s3='ssh -X root@s3'
alias s4='ssh -X root@s4'
alias s5='ssh -X root@s5'
alias s6='ssh -X root@s6'
alias s7='ssh -X root@s7'
alias g0='ssh -X root@g0'
alias g1='ssh -X root@g1'
alias g2='ssh -X root@g2'
alias g3='ssh -X root@g3'
alias g4='ssh -X root@g4'
alias g5='ssh -X root@g5'
alias g6='ssh -X root@g6'
alias g7='ssh -X root@g7'
alias g8='ssh -X root@g8'
alias g9='ssh -X root@g9'
alias fs='ssh -X root@fs'
alias nfs='ssh -X root@nfs'
alias ftp='ssh -X root@ftp'
alias smb='ssh -X root@smb'
alias www='ssh -X root@www'
alias log='ssh -X root@log'
alias mon='ssh -X root@mon'
alias d1='ssh -X root@desktop1'
alias dev1='ssh -X root@dev1'
alias instructor='ssh -X root@instructor'
